package com.example.springboot.controller;

import com.example.springboot.dto.User;
import org.springframework.web.bind.annotation.*;

/**
 * @author Mahendra Prajapati
 * @project spring-boot
 * @since 24-10-2020
 */

@RestController
@RequestMapping("/v1/myApp")
public class MyController {

    @GetMapping("/getUser")
    public String sayHello() {
        return "Hello";
    }

    @PostMapping("/addUser")
    public User addUser(@RequestBody User user) {
        return user;
    }

}
