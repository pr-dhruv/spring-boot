package com.example.springboot.dto;

import lombok.*;

/**
 * @author Mahendra Prajapati
 * @project spring-boot
 * @since 24-10-2020
 */

@NoArgsConstructor
@AllArgsConstructor
@Data
public class User {

    private String userName;
    private String userEmail;
    private String password;

    @Override
    public String toString() {
        return "User : {" +
                "userName=" + userName + '\"' +
                ", userEmail=" + userEmail + '\"' +
                ", password=" + password + '\"' +
                '}';
    }
}
